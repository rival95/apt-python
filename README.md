# **Advanced Programming Techniques: Python** #
## Coding Homework ##
## **Content** ##
* Depth First Search (DFS) for directed graph
* Breadth First Search (BFS) for directed graph
* Cycle Detection using DFS
* Determine whether an undirected graph is bipartite, using DFS
* Determine whether an undirected graph is bipartite, using BFS.
* Implement topological sort using BFS
* Implement topological sort using DFS


### [Link to Requirements](https://bitbucket.org/rival95/apt-python/src/master/APT_Python/src/hw6/hw6.txt)
### How to run:
   Start via: `hw6.hw6.py`
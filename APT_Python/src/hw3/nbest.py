#! /usr/bin/env python

import itertools
import random
import time
from heapq import heappush, heapreplace, heappop

class PQEntry:

    def __init__(self, value):
        self.value = value

    def __cmp__(self, other):
        return mycmp(self.value, other.value)
    
    def negate(self):
        x, y = self.value
        self.value = (x * -1, y * -1)
        
     
def generateLists():
    a = [random.randint(0, 10000) for i in xrange(5000)]
    b = [random.randint(0, 10000) for i in xrange(5000)]
    return a, b

def qselect(k_smallest, array):
    if array == []: return None
    
    pivot = random.choice(array)
    a, b = pivot
    # (x,y) < (x',y') iff. x+y < x'+y' or (x+y==x'+y' and y<y')
    lesser = []
    greater = []
    for x, y in array:
        if x + y < a + b or (x + y == a + b and y < b):
            lesser.append((x, y))
        else:
            greater.append((x, y))
    
    lesser_len = len(lesser)
    
    if k_smallest <= lesser_len: 
        x = qselect(k_smallest, lesser)
        return x
    elif k_smallest == lesser_len + 1:
        lesser.append(pivot) 
        return lesser
    else: 
        lesser.extend(qselect(k_smallest - lesser_len , greater))
        return lesser

def mycmp(this, that):
    # x + y < a + b or (x + y == a + b and y < b)
    x, y = this
    a, b = that
    
    if x + y < a + b:
        return -1
    elif (x + y == a + b and y < b): 
        return -1
    else: 
        return 1

def nbesta(a, b):  # total complexity: O(n*k + n log n + n) = O(n*k + n log n)
    '''
    >>> a, b = [4, 1, 5, 3], [2, 6, 3, 4]
    >>> nbesta(a, b)  
    [(1, 2), (1, 3), (3, 2), (1, 4)]
    '''
    k = len(a)
    c = [element for element in itertools.product(a, b)]  # O(n*k)
    c = sorted(c, cmp=mycmp)  # O(n log n)
    print c[:k]  # O(n)

def nbestb(a, b):  # total complexity: O(n*k + n) = O(n*k + n)
    '''
    >>> a, b = [4, 1, 5, 3], [2, 6, 3, 4]
    >>> nbestb(a, b) 
    [(1, 2), (1, 3), (3, 2), (1, 4)]
    '''
    k = len(a)
    c = [element for element in itertools.product(a, b)]  # O(n*k)
    print qselect(k, c)  # O(n)

def nbestc(a, b):  # total complexity: O(n*k * n) = O(n^2*k
    '''
    >>> a, b = [4, 1, 5, 3], [2, 6, 3, 4]
    >>> nbestc(a, b)
    [(1, 2), (1, 3), (3, 2), (1, 4)]
    '''
    k = len(a)
    p = []
    
    for x, y in itertools.product(a, b):  # O(n*k)
        element = PQEntry((-1 * x, -1 * y))
        if len(p) < k: 
            heappush(p, element)  # O(n)  
        elif element > p[0]: 
            heapreplace(p, element)  # O(n)

    p = [heappop(p).value for i in range(len(p))]  # O(n)
    p.reverse()  # O(n)
    p = [(v[0] * -1, v[1] * -1) for i, v in enumerate(p)]  # O(n)
    print p  # O(n)

if __name__ == "__main__":
    a, b = generateLists()
    # print 'nbesta'
    # t = time.time()
    # nbesta(a, b)
    # print time.time() - t
    print 'nbestb'
    t = time.time()
    nbestb(a, b)
    print time.time() - t
    # print 'nbestc'
    # t = time.time()
    # nbestc(a, b)
    # print time.time() - t
    
# python -m doctest -v filename.py

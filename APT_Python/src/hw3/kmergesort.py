#! /usr/bin/env python

from heapq import merge
from itertools import islice

def chunks(l, n):
    result = []
    for i in xrange(0, n):
        result.append(list(islice(l, i, None, n)))  # O(n)
        
    return result

def kmergesort(mylist, k):  # O(n + n log k) = O(n log k)
    '''
    >>> kmergesort([4,1,5,2,6,3,7,0], 3) 
    [0, 1, 2, 3, 4, 5, 6, 7]
    '''
    if len(mylist) <= 1: return mylist

    left, middle, right = chunks(mylist, k)  # O(n)

    left = kmergesort(left, k)
    middle = kmergesort(middle, k)
    right = kmergesort(right, k)
       
    return _merge(left, middle, right)  # O(n log k)

def _merge(left, middle, right):
    return list(merge(left, middle, right))  # O(n log k), k = # of lists to merge



# python -m doctest -v filename.py

    

#! /usr/bin/env python

import sys
from heapq import heappush, heapreplace, heappop

k = int(sys.stdin.readline().strip())
h = []

for line in sys.stdin:  # O(l); l = # of lines in file
    item = int(line.strip()) * -1
        
    if len(h) < k: heappush(h, item)  # O(log n)
    elif item > h[0]: heapreplace(h, item)  # O(log n)

h = [-1 * heappop(h) for i in range(len(h))]  # O(n)
h.reverse()  # O(n)
print ' '.join(map(str, h))  # O(n)


# O(l * log n + 3 * n) = O(l * log n)


# python -m doctest -v filename.py

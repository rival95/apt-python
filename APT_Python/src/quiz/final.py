#! /usr/bin/env python

graph = {'A': ['B', 'C', 'Z'],
         'B': ['A', 'D'],
         'C': ['A', 'D'],
         'D': ['B', 'C', 'E', 'F'],
         'E': ['D', 'F', 'G'],
         'F': ['D', 'E'],
         'G': ['E'],
         'Z': ['A']}

discover = {}
back = {}
timestamp = 1
criticals = set()

def dfs(v, parent):
    global timestamp
    discover[v] = timestamp
    timestamp += 1
    back[v] = discover[v]  # base
    children = 0  # number of children in dfs, spanning tree
    
    for u in graph[v]:
        if u != parent:
            if u in discover:  # back edge
                back[v] = min(back[v], discover[u])
            else:  # tree edge
                dfs(u, v)  # recursive visit child u
                back[v] = min(back[v], back[u])
                children += 1
                if parent is not None and back[u] >= discover[v] and v not in criticals:
                    # v is not parent and has one child u that does not trace back
                    print "%s is critical" % v
                    criticals.add(v)
                if back[u] >= discover[v]:
                    print '%s-%s is critical edge' % (v, u)
                    print "dead end"
    if parent is None:  # v is root of DFS
        if children > 1:
            print "%s is critical " % v
    timestamp += 1  # finish time


if __name__ == "__main__":
    dfs('A', None)
    print discover
    print back

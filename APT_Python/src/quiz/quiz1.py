def sort(array):
    '''
    >>> sort([4,2,6,3,5,7,1,9])
    [[[[], 1, []], 2, [[], 3, []]], 4, [[[], 5, []], 6, [[], 7, [[], 9, []]]]]
    >>> sort([4, 2, 6, 3, 5, 7, 1, 9, 38, 27, 43, 82, 10])
    [[[[], 1, []], 2, [[], 3, []]], 4, [[[], 5, []], 6, [[], 7, [[], 9, [[[[], 10, []], 27, []], 38, [[], 43, [[], 82, []]]]]]]]
    '''
    if array == []:
        return []
    else:
        pivot = array[0]
        left = [x for x in array if x < pivot ]
        right = [x for x in array[1:] if x >= pivot]
        return [sort(left)] + [pivot] + [sort(right)]

def tree_print(tree, level=None):
    if tree == []: return
    if level is None: level = 0
    
    left, middle, right = tree
    print "\t"*level, middle
    tree_print(left, level + 1)
    tree_print(right, level + 1)

def _qselect(index, tree):
    if tree == []:
        return 0, None
    left, node, right = tree
    num, x = _qselect(index, left)
    if index <= num:
        return index, x
    if index == num + 1:
        return index, node
    num2, y = _qselect(index - num - 1, right)
    return num + num2 + 1, y

def qselect(index, tree):
    return _qselect(index, tree)[1]

def _fib(n, cache={}):
    if cache is None:
        cache = { 0: 1, 1: 1}
    cache[n] = 1 if n < 2 else _fib(n - 1, cache) + _fib(n - 2, cache)
    return cache[n]

def _longest(tree):
    if tree == []:
        return None, 0
    left, middle, right = tree 
    l, ldepth = _longest(left)
    r, rdepth = _longest(right)
 
    return l if ldepth > rdepth else r, max(ldepth, rdepth) + 1

longest = lambda tree: _longest(tree)[1] - 1

class memoize:
    def __init__(self, f):
        self.f = f
        self.dict = {}

    def __call__(self, *args):
        if not args in self.dict:
            self.dict[args] = self.f(*args)

        return self.dict[args]

@memoize
def fib(n):
    if n < 2:
        return 1

    return fib(n - 1) + fib(n - 2)  # turn expensive 

if __name__ == '__main__':
    fib(9)

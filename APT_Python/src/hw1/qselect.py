#! /usr/bin/env python

import random

def qselect(k_smallest, array):
    '''
    >>> qselect(2, [3, 10, 4, 7, 19])
    4
    >>> qselect(4, [11, 2, 8, 3])
    11
    '''
    if array == []: return None
    
    pivot = random.choice(array)
    lesser = [x for x in array if x < pivot ]
    greater = [x for x in array if x > pivot]
    lesser_len = len(lesser)
    
    if k_smallest <= lesser_len : return qselect(k_smallest, lesser)
    elif k_smallest > lesser_len + 1 : return qselect(k_smallest - lesser_len - 1 , greater)
    else: return pivot
    
    
# python -m doctest -v filename.py
    
    

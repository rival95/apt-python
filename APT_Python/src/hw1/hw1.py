#! /usr/bin/env python

def fib(n):
    return n if n < 2 else fib (n - 1) + fib (n - 2)

def mem_fib(n, fibs=None):
    if fibs is None:
        fibs = {0:1, 1:1}
    if n not in fibs:
        fibs[n] = mem_fib(n - 1, fibs) + mem_fib(n - 2, fibs)
    return fibs[n]


def merge_sort(mylist):
    '''
    >>> merge_sort([4, 2, 6, 3, 5, 7, 1, 9, 38, 27, 43, 82, 10])
    [1, 2, 3, 4, 5, 6, 7, 9, 10, 27, 38, 43, 82]
    '''
    if len(mylist) < 2: return mylist

    middle = len(mylist) / 2
    left = mylist[:middle]
    right = mylist[middle:]

    left = merge_sort(left)
    right = merge_sort(right)
       
    return _merge(left, right)

def _merge(left, right):
    left_idx, right_idx = 0, 0
    tmp = []
    
    while(True):
        l = left[left_idx] if left_idx < len(left) else None
        r = right[right_idx] if right_idx < len(right) else None
        
        if (l and r and l < r) or (l and r is None):
            tmp.append(l);
            left_idx += 1
        elif (r and l and r <= l) or (r and l is None):
            tmp.append(r)
            right_idx += 1
        else: break
    
    return tmp
    
# python -m doctest -v filename.py

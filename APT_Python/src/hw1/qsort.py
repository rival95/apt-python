#! /usr/bin/env python

_LEFT = 0
_MIDDLE = 1
_RIGHT = 2


def sort(array):
    '''
    >>> sort([4,2,6,3,5,7,1,9])
    [[[[], 1, []], 2, [[], 3, []]], 4, [[[], 5, []], 6, [[], 7, [[], 9, []]]]]
    >>> sort([4, 2, 6, 3, 5, 7, 1, 9, 38, 27, 43, 82, 10])
    [[[[], 1, []], 2, [[], 3, []]], 4, [[[], 5, []], 6, [[], 7, [[], 9, [[[[], 10, []], 27, []], 38, [[], 43, [[], 82, []]]]]]]]
    '''
    if array == []:
        return []
    else:
        pivot = array[0]
        left = [x for x in array if x < pivot ]
        right = [x for x in array[1:] if x >= pivot]
        return [sort(left)] + [pivot] + [sort(right)]
 
def sorted(tree):
    '''
    >>> sorted(sort([4,2,6,3,5,7,1,9]))
    [1, 2, 3, 4, 5, 6, 7, 9]
    >>> sorted(sort([4, 2, 6, 3, 5, 7, 1, 9, 38, 27, 43, 82, 10]))
    [1, 2, 3, 4, 5, 6, 7, 9, 10, 27, 38, 43, 82]
    '''
    if tree == []: return
    left, middle, right = tree

    array = []
    if [ x for x in [ sorted(left) ] if (x) ] : array.extend(x)
    array.extend([middle])
    if [ x for x in [ sorted(right) ] if (x) ] : array.extend(x)
    
    return array

def search(tree, needle):
    '''
    >>> tree = sort([4,2,6,3,5,7,1,9])
    >>> search(tree, 6)
    True
    >>> search(tree, 6.5)
    False
    '''
    subTree = _search(tree, needle)
    return False if subTree == [] else True

def _search(tree, needle):
    '''
    >>> tree = sort([4,2,6,3,5,7,1,9])
    >>> _search(tree, 3)
    [[], 3, []]
    >>> _search(tree, 0)
    []
    >>> _search(tree, 6.5)
    []
    >>> _search(tree, 0) is _search(tree, 6.5)
    False
    >>> _search(tree, 0) == _search(tree, 6.5)
    True
    '''
    if tree == []: return tree
    left, middle, right = tree
    
    if middle == needle: return tree
    elif needle < middle: return _search(left, needle)
    elif needle > middle: return _search(right, needle)
    
def insert(tree, data):
    '''
    >>> tree = sort([4,2,6,3,5,7,1,9])
    >>> insert(tree, 6.5)
    >>> tree
    [[[[], 1, []], 2, [[], 3, []]], 4, [[[], 5, []], 6, [[[], 6.5, []], 7, [[], 9, []]]]]
    >>> insert(tree, 3)
    >>> tree
    [[[[], 1, []], 2, [[], 3, []]], 4, [[[], 5, []], 6, [[[], 6.5, []], 7, [[], 9, []]]]]
    '''
    subTree = _search(tree, data) 
    if subTree == []: subTree.extend([[], data, []])
    
def find_largest(tree):
    return tree if tree[_RIGHT] == [] else find_largest(tree[_RIGHT])
    
def find_smallest(tree):
    return tree if tree[_LEFT] == [] else find_smallest(tree[_LEFT])

def numOfChildren(node):
    children = 0
    if node[_LEFT] != []: children += 1
    if node[_RIGHT] != []:children += 1 
    return children

def peek(tree, needle):
    return tree[_MIDDLE] == needle

def rearrange(tree, needle, replace):
    '''find the needle in the tree and replace it with "replace"'''
    left, middle, right = tree
    
    if needle == middle:
        return replace
    elif needle > middle:
        if peek(right, needle): right = replace
        else: return rearrange(right, needle, replace)
    else:  # needle < middle
        if peek(left, needle): left = replace
        else: return rearrange(left, needle, replace)
    
    return [left, middle, right]

def _delete(tree, needle):
    left, middle, right = tree
    children = numOfChildren(tree)
    
    if children == 0:
        return []
    elif children == 1:
        return left if left != [] else right        
    else:  # 2 children
        x = find_largest(left)
        middle = x[_MIDDLE]
        left = rearrange(left, middle, x[_LEFT])
        return [left, middle, right]

def delete(tree, needle):
    '''
    >>> tree = sort([4,2,6,3,5,7,1,9])
    >>> insert(tree, 6.5)
    >>> tree
    [[[[], 1, []], 2, [[], 3, []]], 4, [[[], 5, []], 6, [[[], 6.5, []], 7, [[], 9, []]]]]
    >>> tree = delete(tree, 4)
    >>> tree
    [[[[], 1, []], 2, []], 3, [[[], 5, []], 6, [[[], 6.5, []], 7, [[], 9, []]]]]
    '''
    left, middle, right = tree
    
    if needle == middle:
        return _delete(tree, needle)
    elif needle > middle:
        if peek(right, needle): right = _delete(right, needle)
        else: right = delete(right, needle)
    else:  # needle < middle
        if peek(left, needle): left = _delete(left, needle)
        else: left = delete(left, needle)
    
    return [left, middle, right]

def tree_print(tree, level=None):
    if tree == []: return
    if level is None: level = 0
    
    left, middle, right = tree
    print "\t"*level, middle
    tree_print(left, level + 1)
    tree_print(right, level + 1)


# python -m doctest -v filename.py

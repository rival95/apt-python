#! /usr/bin/env python
import random
'''
Given an array A of n numbers, a query x, and a number k, 
   find the k numbers in A that are closest (in value) to x. 
   For example:
   
   find([4,1,3,2,7,4], 5.2, 2)    returns   [4,4]
   find([4,1,3,2,7,4], 6.5, 3)    returns   [4,7,4]
'''

def qselect(k_smallest, array):
    if array == []: return None
    
    pivot = random.choice(array)
    lesser = [x for x in array if x < pivot ]
    greater = [x for x in array if x > pivot]
    dups = array.count(pivot) - 1
    lesser_len = len(lesser)
    
    if k_smallest <= lesser_len : return qselect(k_smallest, lesser)
    elif k_smallest == lesser_len + 1 or k_smallest == lesser_len + 1 + dups:return pivot 
    else: return qselect(k_smallest - lesser_len - 1 - dups , greater)

def find(data, query, k):
    '''
    >>> find([4,1,3,2,7,4], 5.2, 2)
    [4, 4]
    >>> find([4,1,3,2,7,4], 6.5, 3)
    [7, 4, 4]
    '''
    values = []
    if k > len(data): return values
    diff = [abs(x - query) for x in data]
    closest = qselect(k, diff)
    indices = [idx for idx, val in enumerate(diff) if val < closest]
    
    if len(indices) > 0:
        v = [val for idx, val in enumerate(data) if idx in indices]
        values.extend(v)  
    
    k = k - len(indices)
    if k > 0:
        indices = [idx for idx, val in enumerate(diff) if val == closest]
        v = [val for idx, val in enumerate(data) if idx in indices]
        values.extend(v)
    
    return values
    
    
    
    
# python -m doctest -v filename.py
#! /usr/bin/env python
from _collections import defaultdict

class Node(object):
    
    def __init__(self, value, left=None, right=None, parent=None):
        __slots__ = "value", "left", "right", "parent"
        self.value = value
        self.left = left
        self.right = right
        self.parent = parent
        
    def __str__(self):
        return self.value

def createTestTree():
    # left side level 3
    g = Node("g")
    h = Node("h")
    
    # left side level 2
    d = Node("d", g, h)
    g.parent = d
    h.parent = d
    e = Node("e")

    # left side level 1
    b = Node("b", d, e)
    d.parent = b
    e.parent = b
    
    # right side level 2
    f = Node("f")
    
    # right side level 1
    c = Node("c", None, f)
    f.parent = c
    
    # root node
    a = Node("a", b, c)
    b.parent = a
    c.parent = a

    return {'a':a, 'b':b, 'c':c, 'd':d, 'e':e, 'f':f, 'g':g, 'h':h}

def _lca(tree, x, d):
    if tree is None: return None, 0, 0
    
    l, ln, ld = _lca(tree.left, x, d)
    r, rn, rd = _lca(tree.right, x, d)
    
    if(l is None and r) or (l and r is None):  # 1 node found, check if this is 2nd node
        if(tree.value == x.value or tree.value == d.value):  # this is 2nd node
            if l: return tree, ln + 1, ld + 1 if tree.parent is not None else ld
            else: return tree, rn + 1, rd + 1 if tree.parent is not None else rd
        else:  # this is not 2nd node
            if l: return l, ln, ld + 1 if ln < 2 else ld
            else: return r, rn, rd + 1 if rn < 2 else rd
            
    elif tree.value == x.value or tree.value == d.value:  # 1st node found
        return tree, 1, 1
    elif l is None and r is None:
        return None, 0, 0
    else: return tree, ln + rn, ld + rd
        
def lca_2(tree, x, d):
    '''lowest common ancestor: 2 recursion method
    >>> tree = createTestTree()
    >>> print lca_2(tree['a'], tree['a'], tree['g'])
    ['a', 3]
    >>> print lca_2(tree['a'], tree['e'], tree['g'])
    ['b', 3]
    '''
    node, numNodes, depth = _lca(tree, x, d)
    
    if numNodes < 2:
        return [None, -1]
    else:
        return [node.value, depth]

def bfs_order(tree):
    thislevel = [tree]
    lvl = 0
    d = defaultdict()
    
    while thislevel:
        nextlevel = list()
        values = ['']
        for n in thislevel:
            values.append(n.value if n is not None else None)
            if n is None: continue
            nextlevel.append(n.left)
            nextlevel.append(n.right)
        d[lvl] = values
        lvl += 1
        thislevel = nextlevel
    return d

def find(node, bfs):
    for key, value in bfs.iteritems():
        # 0: 'a'
        # 1: 'b', 'c'
        if node.value not in value: continue
        else: return key, value.index(node.value)

    return None, -1

def findParent(lvl, pNum, bfs):
    parents = bfs[lvl]
    
    for i, val in enumerate(parents):
        if i == 0 or parents[i] is None: continue
        elif pNum == 1: return parents[i]
        else: pNum -= 1
    
    return None

def pathToRoot(node, bfs):
    lvl, idx = find(node, bfs)
    path = [node.value]
    
    while lvl > 0:
        lvl -= 1
        idx = (idx + (idx % 2)) / 2
        x = findParent(lvl, idx, bfs)
        path.append(x)
   
    return path
    
def find_lca(xPath, yPath):
    lca_2 = next((obj for obj in xPath if obj in yPath), None)
    if lca_2 is not None:
        indexX = xPath.index(lca_2)
        indexY = yPath.index(lca_2)
        return [lca_2, (indexX + indexY)]
    else: return None, -1
    
def lca_1(tree, x, d):
    '''lowest common ancestor: 1 recursion method
    >>> tree = createTestTree()
    >>> print lca_1(tree['a'], tree['a'], tree['g'])
    ['a', 3]
    >>> print lca_1(tree['a'], tree['e'], tree['g'])
    ['b', 3]
    '''
    bfs = bfs_order(tree)
    xPath = pathToRoot(x, bfs)
    yPath = pathToRoot(d, bfs)
    return find_lca(xPath, yPath)

def triples(data):
    '''
    >>> print triples([1, 2, 3, 3, 2, 4, 2, 6])
    [[1, 2], [1, 3], [1, 3], [1, 2], [1, 2], [2, 2], [2, 4], [2, 2], [3, 3], [2, 4], [2, 2], [4, 2]]
    '''
    if len(data) < 3:return
    xIndex = 0
    trips = []
    
    while xIndex < (len(data) - 1):  
        yIndex = xIndex + 1
        while yIndex < (len(data)):
            if((data[xIndex] + data[yIndex]) in data):
                trips.append([data[xIndex], data[yIndex]])
            yIndex += 1

        xIndex += 1
    return trips

def triples_hash(data):
    '''
    # each key in the dict is a possible X, and each element in
    the key's value is a possible Y; Z is not necessary to to store
    >>> print triples_hash([1, 2, 3, 3, 2, 4, 2, 6])
    {1: set([2, 3]), 2: set([2, 4]), 3: set([3]), 4: set([2]), 6: set([])}
    '''
    if len(data) < 3:return
    trips = dict()

    for i, x in enumerate(data):
        if x in trips: continue
        else: 
            trips[x] = {d for d in data[i + 1:] if (x + d) in data}
    return trips
 
# python -m doctest -v filename.py


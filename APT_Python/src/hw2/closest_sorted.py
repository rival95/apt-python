#! /usr/bin/env python
'''
Now what if the input array is sorted? Can you do it faster?
   Hint1: you can do O(log n + k) time.
   Hint2: what can you do when you're given a sorted array?

   find([1,2,3,4,4,7], 5.2, 2)    returns   [4,4]
   find([1,2,3,4,4,7], 6.5, 3)    returns   [7,4,4]
'''

from bisect import bisect_left

def find(data, query, k):
    '''
    >>> find([1,2,3,4,4,7], 5.2, 2)
    [4, 4]
    >>> find([1,2,3,4,4,7], 6.5, 3)
    [7, 4, 4]
    '''
    values = []
    idx = bisect_left(data, query)
    diff = [abs(x - query) for x in data]
    if idx > 0 and diff[idx] > diff[idx - 1]: idx -= 1
    values.append(data[idx])
    k -= 1
    l = idx - 1
    r = idx + 1
    
    while k > 0:
        if l < 0: l = None
        if r >= len(data): r = None
        
        if(l is None and r is None): break  # no more values
        elif l is not None and r is None:  # can only go left
            values.append(data[l])
            l -= 1
            k -= 1
        elif l is None and r is not None:  # can only go right
            values.append(data[r])
            r += 1
            k -= 1
        else:  # can go both left and right
            values.append(data[l] if diff[l] <= diff[r] else data[r])
            if diff[l] <= diff[r]: l -= 1 
            else: r += 1
            k -= 1
    
    return values
    
    
    
 
# python -m doctest -v filename.py

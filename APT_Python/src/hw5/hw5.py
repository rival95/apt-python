#! /usr/bin/env python


def bsts_bottomup(n, cache=None):
    '''
    >>> bsts_bottomup(3)
    5
    >>> bsts_bottomup(5)
    42
    '''
    if cache == None: cache = {0:1}
    
    for i in range(1, n + 1):
        cache[i] = sum(cache[j - 1] * cache[i - j] for j in range(1, i + 1))
    return cache[n]

def bsts_topdown(n, cache=None):
    '''
    >>> bsts_topdown(3)
    5
    >>> bsts_topdown(5)
    42
    '''
    if cache == None: cache = {0:1}
    if n in cache: return cache[n]
    
    cache[n] = sum(bsts_topdown(i - 1, cache) * bsts_topdown(n - i, cache) for i in range(1, n + 1))
    return cache[n]

def max_wis_bottomup(data):
    '''
    >>> max_wis_bottomup([7, 8, 5])
    (12, [7, 5])
    >>> max_wis_bottomup([-1, 8, 10])
    (10, [10])
    >>> max_wis_bottomup([])
    (0, [])
    '''
    if data == []: 
        print (0, [])
        return
    best = [0] * len(data)
    best[0] = max(0, data[0])
    took = [False] * len(data)
    took[0] = best[0] == data[0]

    _max_wis_bottomup(data, best, took)
    
    path = max_wis_path(data, took)

    print (sum(path), path)

def _max_wis_bottomup(data, best, took):   
    for i in range(1, len(data)):
        best[i] = max(best[i - 1], best[i - 2] + data[i])
        took[i] = best[i] != best[i - 1]

def max_wis_topdown(data):
    '''
    >>> max_wis_topdown([7, 8, 5])
    (12, [7, 5])

    >>> max_wis_topdown([-1, 8, 10])
    (10, [10])

    >>> max_wis_topdown([])
    (0, [])
    '''
    if data == []: 
        print (0, [])
        return
    best = [None] * len(data)
    best[0] = max(0, data[0])
    took = [False] * len(data)
    took[0] = best[0] == data[0]
    
    _max_wis_topdown(data, len(data) - 1, best, took)
    path = max_wis_path(data, took)

    print (sum(path), path)

def _max_wis_topdown(data, i, best, took):
    if i < 0: return 0
    if best[i] != None: return best[i]
    
    best[i] = max(_max_wis_topdown(data, i - 1, best, took), _max_wis_topdown(data, i - 2, best, took) + data[i])
    took[i] = best[i] != best[i - 1]
    
    return best[i]

def max_wis_path(data, took):
    path = []
    i = len(took) - 1
    
    while i > -1:
        if took[i]: 
            path.insert(0, data[i])
            i -= 2
        else: i -= 1
        
    # if took[0] == False: path.insert(0, 0)
    return path

def num_yes_topdown(n):
    '''
    >>> num_yes_topdown(3)
    3
    '''
    print _num_yes_topdown(n)

def _num_yes_topdown(n, cache=None):
    if n < 1: return 0
    if cache == None:
        cache = {1:0, 2:1}
    if n in cache: return cache[n]
    
    ex = 2 ** (n - 2)
    cache[n] = _num_yes_topdown(n - 1, cache) + _num_yes_topdown(n - 2, cache) + ex
    return cache[n]
    
def num_no_topdown(n):
    '''
    >>> num_no_topdown(3)
    5
    '''
    yes = _num_yes_topdown(n)
    print 2 ** n - yes
    
def num_yes_bottomup(n):
    '''
    >>> num_yes_bottomup(3)
    3
    '''
    print _num_yes_bottomup(n)
    
def _num_yes_bottomup(n):
    if n < 1: return 0
    cache = {1:0, 2:1}
    if n in cache: return cache[n]

    for i in range(3, n + 1):            
        cache[i] = cache[i - 1] + cache[i - 2] + 2 ** (i - 2)
    return cache[n]

def num_no_bottomup(n):
    '''
    >>> num_no_bottomup(3)
    5
    '''
    yes = _num_yes_bottomup(n)
    print 2 ** n - yes
    
if __name__ == "__main__":
    import doctest
    doctest.testmod()

# python -m doctest -v filename.py

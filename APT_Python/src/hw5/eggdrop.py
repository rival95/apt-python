#! /usr/bin/env python

import math

def drops(floors, eggs):
    '''
    >>> drops(10, 2)
    4
    >>> drops(15, 4)
    4
    >>> drops(100, 2)
    14
    >>> drops(100, 3)
    9
    '''
    best = {}
    print _drops(floors, eggs, best)

def _drops(floors, eggs, best):
    if floors == 1 or floors == 0: return floors
    if eggs == 1: return floors    
    if (floors, eggs) in best: return best[floors, eggs]
       
    best[floors, eggs] = min(max(_drops(floors - i, eggs, best), _drops(i - 1, eggs - 1, best)) + 1 for i in range(1, floors + 1))
    return best[floors, eggs]

def getStartFloor(n, k):
    x = -1 + math.sqrt(-4 * (-1 * k * n))
    x = int(round(x / 2))
    return x

def backtrace(floors, eggs):
    dt(floors, eggs, 1, 1, 0)

def dt(floors, eggs, dropNum, eggNum, offset):
    t = '   ' * (dropNum - 1) 
    if floors == 0: return
    if floors == 1:
        print t, 'drop egg #', eggNum, 'from floor', floors + offset, '- drop #', dropNum
        return
    if eggs == 1:
        dropTotals = dropNum + floors - 1
        print t, 'drop egg #', eggNum, 'from floor', ", ".join(str(e) for e in range(offset + 1, floors + offset + 1)), 'drop #', dropNum, '-', dropTotals
        return
    
    index = getStartFloor(floors, eggs)
    floor = index + offset
    
    print t, 'drop egg #', eggNum, 'from floor', floor, '- drop #', dropNum
    if index - 1 > 0:
        print t, 'if egg breaks:'
        dt(index - 1, eggs - 1, dropNum + 1, eggNum + 1, offset)
    if floors - index > 0:
        print t, 'else:'
        dt(floors - index, eggs, dropNum + 1, eggNum, floor)
 
if __name__ == "__main__":
    n = 10
    k = 2
    backtrace(n, k)
    # import doctest
    # doctest.testmod()
    

Justin Murray
11/9/2014
Advanced Programming: Python

1. Maximum Weighted Independent Set on a Path Graph
   What's the complexity?
	top_down: 	~O(2^n)
	bottom_up:	O(n)

2. Number of n-node BSTs
   What's the complexity of this DP?
	top_down: 	O(n)
	bottom_up:	O(n^2)
   What's the name of this famous number series?
	Catalan number

4. Egg Dropping.
   What's the complexity?
	O(n^2*k)
   
Debriefing (required!): --------------------------

0. What's your name?
	Justin Murray

1. Approximately how many hours did you spend on this assignment?
	8 hours

2. Would you rate it as easy, moderate, or difficult?
	moderate

3. Did you work on it mostly alone, or mostly with other people?
	Alone

4. How deeply do you feel you understand the material it covers (0%–100%)? 
	75%

5. Any other comments?
	
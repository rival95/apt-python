Justin Murray
10/28/2014
Advanced Programming: Python


Debriefing (required!): --------------------------

0. What's your name?
	Justin Murray

1. Approximately how many hours did you spend on this assignment?
	4 hours

2. Would you rate it as easy, moderate, or difficult?
	Easy

3. Did you work on it mostly alone, or mostly with other people?
	Alone

4. How deeply do you feel you understand the material it covers (0%–100%)? 
	95%

5. Any other comments?
	The TA did an excellent job teaching/explaining the homework during class.
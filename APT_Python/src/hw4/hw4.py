#! /usr/bin/env python

from heapq import heappush, heappop

class Tree(object):
    
    def __init__(self, value, left=None, right=None):
        __slots__ = "value", "left", "right"
        self.value = value
        self.left = left
        self.right = right
        
    def __str__(self):
        return self.value

def _find(tree, k):
    if tree == []: return None
    
    node = tree.value
    
    if node == k: return tree
    span = abs(k - node);

    if (k < node and tree.left != None and abs(k - tree.left.value < span)):
        return _find(tree.left, k)
    elif (k > node and tree.right != None and abs(k - tree.right.value < span)):
        return _find(tree.right, k)
    else: return tree

def find(tree, k):
    '''
    >>> t = Tree(5, Tree(3, Tree(2), Tree(4)), Tree(7, Tree(6), Tree(8)))
    >>> find(t, 6.7)
    7
    '''
    tree = _find(tree, k)
    print None if tree == None else tree.value

def _postorder(pre, ino):
    post = []
    root = pre[0]
    left = ino[:ino.index(root)]
    if len(left) > 0: post.extend(_postorder(pre[1:len(left) + 1], left))
    
    right = ino[ino.index(root) + 1:]
    if len(right) > 0: post.extend(_postorder(pre[len(left) + 1:], right))
    
    post.append(root)
    return post

def postorder(pre, ino):
    '''
    >>> postorder('gdafemhz', 'adefghmz')
    ['a', 'e', 'f', 'd', 'h', 'z', 'm', 'g']
    >>> postorder([1,2,3,5], [2,1,3,5])
    [2, 5, 3, 1]
    '''
    print _postorder(pre, ino)
    
def inversions(data):
    '''
    >>> inversions([3,1,4,2])
    3
    >>> inversions([4, 2, 1, 5, 3, 6])
    5
    '''
    print len(_inversions(data)[0])

def _inversions(data):
    if len(data) < 2: return [], data
    
    pivot = len(data) / 2
    left = data[:pivot]
    right = data[pivot:]
    
    l_inv, left = _inversions(left)
    r_inv, right = _inversions(right)
    
    inv, sdata = find_inversions(left, right)
    return l_inv + r_inv + inv, sdata
    
def find_inversions(left, right):
    left_idx, right_idx = 0, 0
    inv, tmp = [], []
    
    while(True):
        l = left[left_idx] if left_idx < len(left) else None
        r = right[right_idx] if right_idx < len(right) else None
        
        if(l and r and l > r):
            inv.append((l, r))
        
        if (l and r and l < r) or (l and r is None):
            tmp.append(l);
            left_idx += 1
        elif (r and l and r <= l) or (r and l is None):
            tmp.append(r)
            right_idx += 1
        else: break
    
    return inv, tmp
    
def _medians(data):
    if len(data) < 2: return data[0]
    minH, maxH = [], []
    
    if data[0] > data[1]:
        heappush(maxH, data[1] * -1)
        heappush(minH, data[0])
    else :
        heappush(minH, data[1])
        heappush(maxH, data[0] * -1)
        
    data = data[2:]
    
    while len(data) > 0:
        d = data[0]
        
        if d < (maxH[0] * -1): heappush(maxH, d * -1)
        else : heappush(minH, d)
        
        balanceTrees(maxH, minH)
        data = data[1:]
        
    if len(maxH) == len(minH):
        return ((maxH[0] * -1) + minH[0]) / 2.0
    elif len(maxH) > len(minH):
        return maxH[0] * -1
    else : return minH[0]

def balanceTrees(maxH, minH):
    if abs(len(maxH) - len(minH)) < 2:
        return 
    
    if len(maxH) > len(minH):
        while len(maxH) > len(minH) + 1:
            d = heappop(maxH) * -1
            heappush(minH, d)
    elif len(minH) > len(maxH):
        while len(minH) > len(maxH) + 1:
            d = heappop(minH)
            heappush(maxH, d * -1)
        
    return
        
def medians(data):
    '''
    >>> medians([5, 3, 4, 1, 6])
    [5, 4.0, 4, 3.5, 4]
    '''
    if data == None or len(data) < 1: return None
    meds = []
    count = 1
    while count <= len(data):
        d = data[:count]
        meds.append(_medians(d))
        count += 1

    print meds

if __name__ == "__main__":
    t = Tree(5, Tree(3, Tree(2), Tree(4)), Tree(7, Tree(6), Tree(8)))
    t = find(t, 6.7)
    postorder('gdafemhz', 'adefghmz')
    inversions([4, 2, 1, 5, 3, 6])
    medians([5, 3, 4, 1, 6])

# python -m doctest -v filename.py

#! /usr/bin/env python

import random

_time = 1
_TREE_EDGE = 0
_BACK_EDGE = 1
_FORWARD_EDGE = 2
_CROSS_EDGE = 3
edges = ["tree", "back", "forward", "cross"]

def dfs_span(tree):
    '''
    >>> tree = {"A": ["B"], "B": ["C", "D", "E"], "C": ["E"], "D": ["A", "E"]}
    >>> dfs_span(tree)
    A -> B 
         B -> C 
              C -> E 
         B -> D 
              *D -> A (back)
              *D -> E (cross)
         *B -> E (forward)
    '''
    global _time
    _time = 1
    vertex = tree.keys()[0]
    visited = {}
    visited[vertex] = (_time, float("inf"))
    _dfs_span(tree, visited, vertex)
    

def _dfs_span(tree, visited, vertex, indent=0):
    global _time
    edges = tree[vertex] if vertex in tree else []
    
    for edge in edges:
        if edge not in visited:
            printEdge(vertex, edge, _TREE_EDGE, indent)
            _time += 1
            visited[edge] = (_time, float("inf"))
            _dfs_span(tree, visited, edge, indent + 1)
        else:
            printEdge(vertex, edge, edgeType(visited, vertex, edge), indent)
            
    discover, _ = visited[vertex]
    _time += 1
    visited[vertex] = (discover, _time)

def printEdge(parent, child, edge, indent):
    t = '     ' * indent
    edgeType = '({})'.format(edges[edge]) if edge > 0 else ''
    star = '*' if edge > 0 else ''
    x = '{}{}{} -> {} {}'.format(t, star, parent, child, edgeType)
    print x 

def edgeType(dfs, parent, child):
    childx, childy = dfs[child]
    parentx, parenty = dfs[parent]
    
    if (childx, childy) == (float("inf"), float("inf")):
        return _TREE_EDGE

    if childx < childy < parentx:
        return _CROSS_EDGE
    
    if childx < parentx:
        return _BACK_EDGE
    
    if parentx < childx < childy < parenty:
        return _FORWARD_EDGE

def bfs_span(tree):
    '''
    >>> tree = {"A": ["B"], "B": ["C", "D", "E"], "C": ["E"], "D": ["A", "E"]}
    >>> bfs_span(tree)
    A B C D E
    back: D->A
    cross: C->E, D->E
    forward: none
    '''
    vertex = tree.keys()[0]
    visited = {}
    visited[vertex] = 1
    queue = ["A"]
    edgeT = {"tree":[], "cross":[], "back":[]}
    visited, edgeTypes = _bfs_span(tree, queue, visited, edgeT)   
    sorted_x = sorted(visited.items(), key=lambda x: (x[1], x[0]))
    a = []
    
    for (x, _) in sorted_x:
        a.append(x)
        
    print ' '.join(a)
    print 'back:', ', '.join(edgeTypes['back'])
    print 'cross:', ', '.join(edgeTypes['cross'])
    print 'forward: none'
    
def _bfs_span(tree, queue, visited, edgeT):
    if queue == []: return
    
    vertex = queue.pop(0)
    edges = tree[vertex] if vertex in tree else []
    depth = visited[vertex]
    
    for edge in edges:
        if edge not in queue and edge not in visited:
            queue.append(edge)
            
        if edge not in visited:
            visited[edge] = depth + 1

        # edge detection 
        edgeType_bfs(visited, vertex, edge, edgeT)
            
    _bfs_span(tree, queue, visited, edgeT)
    return visited, edgeT

def edgeType_bfs(visited, fromNode, toNode, edgeT):
    fromDepth = visited[fromNode]
    toDepth = visited[toNode]
    if fromDepth > toDepth:
        edgeT["back"].append("{}->{}".format(fromNode, toNode))
    elif fromDepth == toDepth:
        edgeT["cross"].append("{}->{}".format(fromNode, toNode))
    elif fromDepth < toDepth:
        edgeT["tree"].append("{}->{}".format(fromNode, toNode)) 

def dfs_cycle(tree, printt=True):
    '''
    >>> tree = {"A": ["B"], "B": ["C", "D", "E"], "C": ["E"], "D": ["A", "E"]}
    >>> dfs_cycle(tree)   
    cycle detected: A->B->D->A
    '''
    global _time
    _time = 1
    vertex = tree.keys()[0]
    visited = {}
    visited[vertex] = (_time, float("inf"))
    nodes, cycle = _dfs_cycle(tree, visited, vertex)
    
    if cycle:
        nodes.append(vertex)
        nodes.reverse()
        if printt: print 'cycle detected:', '->'.join(nodes)
        if not printt: return True
    else:
        if printt: print "ACYCLIC"
        if not printt: return False


def _dfs_cycle(tree, visited, vertex, indent=0):
    global _time
    edges = tree[vertex] if vertex in tree else []
    
    for edge in edges:
        if edge not in visited:
            _time += 1
            visited[edge] = (_time, float("inf"))
            nodes, cycle = _dfs_cycle(tree, visited, edge, indent + 1)
            
            if cycle:
                nodes.append(edge)
                return nodes, True
        else:
            et = edgeType(visited, vertex, edge)
            if et == _BACK_EDGE: 
                nodes = []
                nodes.append(edge)
                return nodes, True
            
    discover, _ = visited[vertex]
    _time += 1
    visited[vertex] = (discover, _time)
    return [], False

def bipartite(graph):
    '''
    >>> tree = {"A": ["D", "E"], "B": ["D"], "C": ["E"]}
    >>> bipartite(tree)
    LEFT: {A, C, B}; RIGHT: {E, D}.
    
    >>> tree = {"A": ["D", "E"], "B": ["D"], "C": ["A", "E"]}
    >>> bipartite(tree)
    NOT BIPARTITE
    '''
    left, right = set(), set()
    vertices = sorted(graph.keys())
    
    for vertex in vertices:
        inRight = vertex in right
        inLeft = vertex in left
        
        if not inRight and not inLeft:
            left.add(vertex)
            inLeft = True
        
        for edge in graph[vertex]:
            if inLeft:  # vertex is in left; nodes must be right only
                if edge in left:
                    print 'NOT BIPARTITE'
                    return
                else: right.add(edge)
            else:  # vertex is in right; nodes must be left only
                if edge in right:
                    print 'NOT BIPARTITE'
                    return
                else: left.add(edge)
    
    left = ', '.join(left)
    right = ', '.join(right)
    print 'LEFT: {' + left + '}; RIGHT: {' + right + '}.'

def topsort_bfs(graph):
    cycle = dfs_cycle(graph, False)
    if cycle:
        print 'NO TOPOLOGICAL ORDER'
        return
    else: toplogical_sort(graph, True)

def topsort_dfs(graph):
    cycle = dfs_cycle(graph, False)
    if cycle:
        print 'NO TOPOLOGICAL ORDER'
        return
    else: toplogical_sort(graph, False)
    
def toplogical_sort(graph, bfs=True):
    top = calc_incoming_edges(graph)
    queue = find_zero_nodes(top)
    order = []

    while len(queue) > 0:
        v = random.choice(queue) if bfs else queue.pop()
        if bfs: queue.remove(v)
        order.append(v)
        edges = graph[v] if v in graph else []
        edges.reverse()
        
        for edge in edges:
            top[edge] -= 1
            
            if top[edge] == 0:
                queue.append(edge)

    print ' -> '.join(order)
    

def find_zero_nodes(graph):
    zeros = []
    
    for v, k in enumerate(graph):
        if v == 0: zeros.append(k)
        
    return zeros
            
def calc_incoming_edges(graph):
    l = {}
    
    for v in graph.keys():
        if v not in l:
            l[v] = 0
        for edge in graph[v]:
            if edge not in l:
                l[edge] = 1
            else: l[edge] += 1
    return l
if __name__ == "__main__":
    # tree = {"A": ["B", "D"], "B": ["C", "G"], "D": ["E"], "E": ["B", "F"], "F": ["C"]}
    # topsort_dfs(tree)
    # topsort_bfs(tree)
    import doctest
    doctest.testmod()

# python -m doctest -v filename.py

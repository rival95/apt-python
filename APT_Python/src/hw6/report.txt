Justin Murray
11/30/2014
Advanced Programming: Python

2. Implement BFS for directed graph.
   print the level-order traversal of the BFS spanning tree,
   and print the list of back, cross, and forward edges:

   A B C D E
   back: D->A
   cross: D->E, C->E
   forward: none

   Is it correct that BFS never produces forward edges? Why?
      Yes, it is correct.  If BFS produced a forward edge then that node would be its child, 
      not a descendant of its child.  Resulting in a tree edge.

8. Which types of edges will the DFS/BFS spanning tree on directed/undirected graph have?

   (a) DFS on directed: tree, back, forward, cross
   (b) BFS on directed: tree, back
   (c) DFS on undirected: tree, back
   (d) BFS on undirected: tree
   
  
Debriefing (required!): --------------------------

0. What's your name?
	Justin Murray

1. Approximately how many hours did you spend on this assignment?
	8 hours

2. Would you rate it as easy, moderate, or difficult?
	moderate

3. Did you work on it mostly alone, or mostly with other people?
	Alone

4. How deeply do you feel you understand the material it covers (0%–100%)? 
	90%

5. Any other comments?
	